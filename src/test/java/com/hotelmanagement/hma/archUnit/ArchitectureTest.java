package com.hotelmanagement.hma.archUnit;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import org.junit.Test;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

@AnalyzeClasses(packages = "com.hotelmanagement.app")
public class ArchitectureTest {

    private final JavaClasses importedClasses = new ClassFileImporter().importPackages("com.hotelmanagement.hma");




    @Test
    public void test_controller_methods() {
        ArchRule myRule = ArchRuleDefinition.methods()
                .that().arePublic()
                .and().areDeclaredInClassesThat()
                .resideInAPackage("..controller..")
                .and().areDeclaredInClassesThat()
                .haveSimpleNameEndingWith("Controller")
                .and().areDeclaredInClassesThat()
                .areAnnotatedWith(RestController.class)
                .should().beAnnotatedWith(PutMapping.class)
                .orShould().beAnnotatedWith(GetMapping.class)
                .orShould().beAnnotatedWith(PostMapping.class)
                .orShould().beAnnotatedWith(DeleteMapping.class);

        myRule.check(importedClasses);

    }

    @Test
    public void test_controller_class() {
        ArchRule myRule = classes().that().resideInAPackage("..controller..")
                .should().beAnnotatedWith(RestController.class).andShould().beAnnotatedWith(RequestMapping.class)
                        .andShould().haveSimpleNameEndingWith("Controller")
                        .andShould().haveOnlyFinalFields()
                        .andShould().dependOnClassesThat().resideInAPackage("..service..");

        myRule.check(importedClasses);
    }

    @Test
    public void test_service_class() {
        ArchRule myRule = classes()
                .that().resideInAPackage("..service..")
                .should().onlyBeAccessed().byAnyPackage("..controller..", "..service..")
                .andShould().onlyHaveDependentClassesThat().resideInAnyPackage("..controller..","..service..")
                .andShould().dependOnClassesThat().resideInAnyPackage("..service..","..repository..")
                .andShould().haveSimpleNameEndingWith("Service")
                .andShould().beAnnotatedWith(Service.class);

        myRule.check(importedClasses);

    }


}
