package com.hotelmanagement.hma.enums;

public enum Gender {

    MAN(0), WOMAN(1), OTHERS(3);

    private final int value;

    Gender(int value) {
        this.value = value;
    }

}
