package com.hotelmanagement.hma.enums;

public enum RoomStatus {


    RESERVED(1), EMPTY(2);

    private final int value;

    RoomStatus(int value) {
        this.value = value;
    }
}
