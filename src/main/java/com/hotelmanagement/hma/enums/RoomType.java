package com.hotelmanagement.hma.enums;

public enum RoomType {


    ECONOMIC(1), COMFORT(2), VIP(3);

    private final int value;

    RoomType(int value) {
        this.value = value;
    }
}
