package com.hotelmanagement.hma.enums;

public enum BookingType {

    ACTIVE(1), EXIT(2);

    private final int value;


    BookingType(int value) {
        this.value = value;
    }
}
