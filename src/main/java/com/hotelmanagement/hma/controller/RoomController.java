package com.hotelmanagement.hma.controller;


import com.hotelmanagement.hma.dto.BookingDto;
import com.hotelmanagement.hma.dto.RoomDto;
import com.hotelmanagement.hma.service.RoomService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/room")
public class RoomController {

    private final RoomService roomService;

    public RoomController(RoomService roomService) {
        this.roomService = roomService;
    }

    @PostMapping("/{hotelId}/create")
    public ResponseEntity<RoomDto> createRoom(@PathVariable Long hotelId, @RequestBody RoomDto roomDto) {
        return new ResponseEntity<>(roomService.saveRoom(hotelId, roomDto), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<RoomDto> getRoom(@PathVariable Long id) {
        return new ResponseEntity<>(roomService.getRoomById(id), HttpStatus.OK);
    }

    @GetMapping("/{id}/bookings")
    public ResponseEntity<List<BookingDto>> geRoomBokings(@PathVariable Long id) {
        return new ResponseEntity<>(roomService.getRoomBookings(id), HttpStatus.OK);
    }

    @GetMapping("/topRooms")
    public ResponseEntity<Page<RoomDto>> getTopRooms(Pageable pageable) {
        return new ResponseEntity<>(roomService.getTopRooms(pageable), HttpStatus.OK);
    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteRoom(@PathVariable Long id) {
        roomService.deleteRoomById(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }


    @PutMapping("/update/{id}")
    public ResponseEntity<RoomDto> updateRoom(@PathVariable Long id, @RequestBody RoomDto roomDto) {
        return new ResponseEntity<>(roomService.updateRoom(id, roomDto), HttpStatus.OK);
    }


}
