package com.hotelmanagement.hma.controller;

import com.hotelmanagement.hma.dto.BookingDto;
import com.hotelmanagement.hma.dto.CustomerDto;
import com.hotelmanagement.hma.dto.requests.CustomerExitRequest;
import com.hotelmanagement.hma.service.BookingService;
import com.hotelmanagement.hma.service.CustomerService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    private final CustomerService customerService;
    private final BookingService bookingService;

    public CustomerController(CustomerService customerService, BookingService bookingService) {
        this.customerService = customerService;
        this.bookingService = bookingService;
    }


    @GetMapping("/customerList")
    public ResponseEntity<Page<CustomerDto>> getAllCustomers(Pageable pageable) {
        return new ResponseEntity<>(customerService.getCustomerPage(pageable), HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<CustomerDto> createCustomer(@RequestBody CustomerDto customerDto) {
        return new ResponseEntity<>(customerService.saveCustomer(customerDto), HttpStatus.CREATED);
    }


    @GetMapping("/{id}")
    public ResponseEntity<CustomerDto> getCustomer(@PathVariable Long id) {
        return new ResponseEntity<>(customerService.getCustomerById(id), HttpStatus.OK);
    }


    @GetMapping("/{customerId}/booking/{bookingId}")
    public ResponseEntity<BookingDto> getCustomerBooking(@PathVariable Long customerId, @PathVariable Long bookingId) {
        return new ResponseEntity<>(bookingService.getBookingByCustomerId(customerId, bookingId), HttpStatus.OK);
    }


    @PutMapping("/exit")
    public ResponseEntity<BookingDto> customerExit(@RequestBody CustomerExitRequest customerExitRequest) {
        return new ResponseEntity<>(customerService.customerExit(customerExitRequest), HttpStatus.OK);
    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteCustomer(@PathVariable Long id) {
        customerService.deleteCustomerById(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }
}
