package com.hotelmanagement.hma.controller;


import com.hotelmanagement.hma.dto.AddressDto;
import com.hotelmanagement.hma.dto.HotelDto;
import com.hotelmanagement.hma.service.HotelService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/hotel")
public class HotelController {

    private final HotelService hotelService;

    public HotelController(HotelService hotelService) {
        this.hotelService = hotelService;
    }

    @PostMapping("/create")
    public ResponseEntity<HotelDto> saveAddress(@RequestBody HotelDto hotelDto) {
        return new ResponseEntity<>(hotelService.saveHotel(hotelDto), HttpStatus.CREATED);
    }


    @PutMapping("/{id}/updateAddress")
    public ResponseEntity<AddressDto> updateAddress(@PathVariable Long id, @RequestBody AddressDto addressDto) {
        return new ResponseEntity<>(hotelService.updateHotelAddress(id, addressDto), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<HotelDto> updateAddress(@PathVariable Long id) {
        return ResponseEntity.ok(hotelService.getHotel(id));
    }

    @GetMapping("/tops")
    public ResponseEntity<List<HotelDto>> updateAddress() {
        return ResponseEntity.ok(hotelService.getTopHotels());
    }


}
