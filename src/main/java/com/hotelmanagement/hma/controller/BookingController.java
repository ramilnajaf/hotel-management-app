package com.hotelmanagement.hma.controller;

import com.hotelmanagement.hma.dto.BookingDto;
import com.hotelmanagement.hma.dto.requests.BookingRequest;
import com.hotelmanagement.hma.service.BookingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = "/booking")
public class BookingController {

    protected final BookingService bookingService;

    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @PostMapping("/create")
    public ResponseEntity<BookingDto> createBooking(@RequestBody BookingRequest bookingRequest) {
        return new ResponseEntity<>(bookingService.saveBooking(bookingRequest), HttpStatus.CREATED);
    }


    @GetMapping("/{id}")
    public ResponseEntity<BookingDto> getBookingById(@PathVariable Long id) {
        return new ResponseEntity<>(bookingService.getBookingById(id), HttpStatus.OK);
    }

    @GetMapping("/withRoomId/{roomId}")
    public ResponseEntity<List<BookingDto>> getBookingCustomerAndRoom(@PathVariable Long roomId) {
        return new ResponseEntity<>(bookingService.getBookingByRoom(roomId), HttpStatus.OK);
    }


}
