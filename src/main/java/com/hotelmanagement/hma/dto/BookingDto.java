package com.hotelmanagement.hma.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@ToString
public class BookingDto {


    private CustomerDto customer;

    @JsonBackReference
    private RoomDto room;
    private BigDecimal commonPrice;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd MM yyyy")
    private LocalDate fromDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd MM yyyy")
    private LocalDate toDate;
    private LocalDate exitDate;

}
