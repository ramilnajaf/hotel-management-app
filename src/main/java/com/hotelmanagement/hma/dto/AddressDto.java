package com.hotelmanagement.hma.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressDto {


    private String country;
    private String city;
    private String street;

    @JsonIgnore
    private HotelDto hotel;
}
