package com.hotelmanagement.hma.dto;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.hotelmanagement.hma.enums.RoomStatus;
import com.hotelmanagement.hma.enums.RoomType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Set;

@Getter
@Setter
@ToString
public class RoomDto {

    private HotelDto hotel;
    private String roomNo;
    private RoomType roomType;
    private RoomStatus roomStatus;
    private Integer roomFloor;
    private BigDecimal roomPrice;

   @JsonManagedReference
    private Set<BookingDto> bookings;


}
