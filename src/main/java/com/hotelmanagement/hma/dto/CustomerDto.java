package com.hotelmanagement.hma.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hotelmanagement.hma.enums.Gender;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@ToString
public class CustomerDto {

    private String name;
    private String surname;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd MM yyyy")
    private LocalDate birthday;
    private String mobile;
    private Gender gender;

    @JsonIgnore
    private Set<BookingDto> bookings;
}
