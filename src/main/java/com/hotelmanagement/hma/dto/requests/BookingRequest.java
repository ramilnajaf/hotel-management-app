package com.hotelmanagement.hma.dto.requests;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDate;


@Getter
@Setter
@ToString
public class BookingRequest {


    private Long hotelId;
    private Long customerId;
    private Long roomId;
    private BigDecimal commonPrice;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd MM yyyy")
    private LocalDate fromDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd MM yyyy")
    private LocalDate toDate;
}
