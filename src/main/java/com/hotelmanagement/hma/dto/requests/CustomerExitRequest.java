package com.hotelmanagement.hma.dto.requests;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
public class CustomerExitRequest {
   private Long customerId;
   private Long bookingId;
}
