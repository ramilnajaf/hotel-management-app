package com.hotelmanagement.hma;

import com.hotelmanagement.hma.repository.RoomRepository;
import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

//    private final RoomRepository roomRepository;

    public DemoApplication() {

//
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

//        Room room = Room.builder().roomNo("1")
//                .roomFloor(2)
//                .roomPrice(BigDecimal.valueOf(100))
//                .roomType(RoomType.ECONOMIC)
//                .roomStatus(RoomStatus.EMPTY)
//                .build();
//
//        roomRepository.save(room);


    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();

    }
}
