package com.hotelmanagement.hma.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hotelmanagement.hma.enums.Gender;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customer_generator")
    @SequenceGenerator(name = "customer_generator", sequenceName = "customer_seq", allocationSize = 1)
    private Long id;

    private String name;

    private String surname;

    private LocalDate birthday;

    private String mobile;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @CreationTimestamp
    private Date dataDate;


    @OneToMany(mappedBy = "customer")
    private Set<Booking> bookings;


}
