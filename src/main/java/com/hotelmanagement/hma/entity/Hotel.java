package com.hotelmanagement.hma.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter

public class Hotel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hotel_generator")
    @SequenceGenerator(name = "hotel_generator", sequenceName = "hotel_seq", allocationSize = 1)
    private Long id;
    private String name;
    private String contactNumber;


    @OneToOne(mappedBy = "hotel")
    private Address address;

    @OneToMany(mappedBy = "hotel")
    private Set<Room> rooms;


}
