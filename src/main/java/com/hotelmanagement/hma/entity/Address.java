package com.hotelmanagement.hma.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@ToString
@Getter
@Setter
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "address_generator")
    @SequenceGenerator(name = "address_generator", sequenceName = "address_seq", allocationSize = 1)
    @Column(name = "id")
    private long id;
    private String country;
    private String city;
    private String street;

    @OneToOne
    @JoinColumn(name = "hotel_id", referencedColumnName = "id", unique = true)
    private Hotel hotel;


}