package com.hotelmanagement.hma.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hotelmanagement.hma.enums.RoomStatus;
import com.hotelmanagement.hma.enums.RoomType;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Setter

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "room_generator")
    @SequenceGenerator(name = "room_generator", sequenceName = "room_seq", allocationSize = 1)
    private Long id;
    private String roomNo;

    @Enumerated(EnumType.STRING)
    private RoomType roomType;
    @Enumerated(EnumType.STRING)
    private RoomStatus roomStatus;

    private Integer roomFloor;

    private BigDecimal roomPrice;

    @CreationTimestamp
    private LocalDate creationTime;


    @OneToMany(mappedBy = "room",fetch = FetchType.LAZY)
    private Set<Booking> bookings;


    @ManyToOne()
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;


}
