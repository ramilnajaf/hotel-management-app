package com.hotelmanagement.hma.entity;


import com.hotelmanagement.hma.enums.BookingType;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Setter
@Getter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "booking_generator")
    @SequenceGenerator(name = "booking_generator", sequenceName = "booking_seq", allocationSize = 1)
    private Long id;
    @ManyToOne()
    @JoinColumn(name = "customer_id")
    private Customer customer;
    @ManyToOne()
    @JoinColumn(name = "room_id")
    private Room room;

    private BigDecimal commonPrice;

    @Enumerated(EnumType.STRING)
    private BookingType bookingType;

    private LocalDate fromDate;

    private LocalDate toDate;
    @CreationTimestamp
    private LocalDate creationTime;


    private LocalDate exitDate;


}
