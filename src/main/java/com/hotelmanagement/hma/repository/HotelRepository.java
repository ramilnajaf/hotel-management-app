package com.hotelmanagement.hma.repository;

import com.hotelmanagement.hma.entity.Hotel;
import com.hotelmanagement.hma.entity.Room;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HotelRepository extends JpaRepository<Hotel, Long> {

    @Query(value = "select  h.id,h.name,h.contact_number from hotel h join room r on h.id= r.hotel_id join booking b on r.id=b.room_Id\n" +
            "group by h.id,h.name,h.contact_number \n" +
            "order by  count(b.id) desc",nativeQuery = true)
    List<Hotel> getTopHotels();
}
