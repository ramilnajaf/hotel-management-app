package com.hotelmanagement.hma.repository;

import com.hotelmanagement.hma.entity.Room;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Pageable;


@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {

    @Query("SELECT r FROM Room r ORDER BY SIZE(r.bookings) DESC")
    Page<Room> getTopRooms(Pageable pageable);
}
