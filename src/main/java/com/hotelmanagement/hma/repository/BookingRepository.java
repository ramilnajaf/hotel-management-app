package com.hotelmanagement.hma.repository;

import com.hotelmanagement.hma.entity.Booking;
import com.hotelmanagement.hma.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Long> {
    List<Booking> findAllByRoom(Room room);

    @Query("SELECT b FROM Booking b WHERE b.room = :room")
    List<Booking> findBookingByRoom(
            @Param("room") Room room);
}
