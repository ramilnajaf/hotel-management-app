package com.hotelmanagement.hma.exception;

import org.springframework.http.HttpStatus;

public class RoomNotAvailableException extends BaseException{

    public RoomNotAvailableException (String msg) {
        super(msg, HttpStatus.CONFLICT);
    }
}
