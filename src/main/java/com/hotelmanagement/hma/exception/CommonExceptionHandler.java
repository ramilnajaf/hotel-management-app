package com.hotelmanagement.hma.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class CommonExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException e) {
        log.error("Illegal argument passed: {}", e.getMessage());
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(BaseException.class)
    public ResponseEntity<ApiError> handleBaseExceptions(BaseException e) {
        log.error("" + e.getClass().getSimpleName() + ": {}", e.getMessage());
        return new ResponseEntity<>(new ApiError(e.getMessage()), e.getHttpStatus());
    }
}
