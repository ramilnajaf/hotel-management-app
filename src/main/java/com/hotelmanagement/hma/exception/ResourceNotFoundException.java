package com.hotelmanagement.hma.exception;

import org.springframework.http.HttpStatus;

public class ResourceNotFoundException extends BaseException{

    public ResourceNotFoundException(String msg) {
        super(msg, HttpStatus.NOT_FOUND);
    }
}
