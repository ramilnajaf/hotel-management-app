package com.hotelmanagement.hma.exception;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class ApiError {
    String title;

    public ApiError(String title) {
        this.title = title;
    }
}
