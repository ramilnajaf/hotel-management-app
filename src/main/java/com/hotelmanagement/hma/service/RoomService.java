package com.hotelmanagement.hma.service;

import com.hotelmanagement.hma.dto.BookingDto;
import com.hotelmanagement.hma.dto.RoomDto;
import com.hotelmanagement.hma.entity.Booking;
import com.hotelmanagement.hma.entity.Hotel;
import com.hotelmanagement.hma.entity.Room;
import com.hotelmanagement.hma.exception.ResourceNotFoundException;
import com.hotelmanagement.hma.repository.BookingRepository;
import com.hotelmanagement.hma.repository.RoomRepository;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoomService {

    private final RoomRepository roomRepository;
    private final ModelMapper modelMapper;
    private final BookingRepository bookingRepository;
    private final HotelService hotelService;

    public RoomService(RoomRepository roomRepository, ModelMapper modelMapper, BookingRepository bookingRepository, HotelService hotelService) {
        this.roomRepository = roomRepository;
        this.modelMapper = modelMapper;
        this.bookingRepository = bookingRepository;
        this.hotelService = hotelService;
    }


    public RoomDto saveRoom(Long hotelId, RoomDto roomDto) {
        Room room = modelMapper.map(roomDto, Room.class);
        Hotel hotel = hotelService.getHotelById(hotelId);

        hotel.getRooms().add(room);
        hotelService.saveHotel(hotel);

        room.setHotel(hotel);
        roomRepository.save(room);

        return modelMapper.map(roomRepository.save(room), RoomDto.class);
    }

    public RoomDto getRoomById(Long id) {
        Room room = getRoomByRoomId(id);
        return modelMapper.map(room, RoomDto.class);
    }


    public void deleteRoomById(Long id) {
        roomRepository.delete(getRoomByRoomId(id));
    }


    public RoomDto updateRoom(Long id, RoomDto roomDto) {
        Room room = getRoomByRoomId(id);
        Room room2 = modelMapper.map(roomDto, Room.class);
        room2.setId(room.getId());
        return modelMapper.map(roomRepository.save(room2), RoomDto.class);
    }

    public List<BookingDto> getRoomBookings(Long id) {
        Room room = getRoomByRoomId(id);
        List<Booking> bookings = bookingRepository.findAllByRoom(room);

        List<BookingDto> bookingDtos = bookings
                .stream()
                .map(bkng -> modelMapper.map(bkng, BookingDto.class))
                .collect(Collectors.toList());

        return bookingDtos;
    }
    public Page<RoomDto> getTopRooms(Pageable pageable){
        Page<Room> rooms = roomRepository.getTopRooms(pageable);
        Page<RoomDto> roomDtos = rooms.map(room -> modelMapper.map(room,RoomDto.class));
        return  roomDtos;
    }

    protected Room getRoomByRoomId(Long id) {
        Room room = roomRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Room is not found with given info"));
        return room;
    }


    protected Room saveRoom(Room room) {
        return roomRepository.save(room);
    }

}




