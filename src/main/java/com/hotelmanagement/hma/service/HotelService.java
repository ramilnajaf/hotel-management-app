package com.hotelmanagement.hma.service;

import com.hotelmanagement.hma.dto.AddressDto;
import com.hotelmanagement.hma.dto.HotelDto;
import com.hotelmanagement.hma.entity.Address;
import com.hotelmanagement.hma.entity.Hotel;
import com.hotelmanagement.hma.exception.ResourceNotFoundException;
import com.hotelmanagement.hma.repository.HotelRepository;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class HotelService {


    private final HotelRepository hotelRepository;
    private final ModelMapper modelMapper;
    private final AddressService addressService;


    public HotelService(HotelRepository hotelRepository, ModelMapper modelMapper, AddressService addressService) {
        this.hotelRepository = hotelRepository;
        this.modelMapper = modelMapper;
        this.addressService = addressService;
    }

    public HotelDto saveHotel(HotelDto hotelDto) {
        Hotel hotel = modelMapper.map(hotelDto, Hotel.class);
        Hotel savedHotel = null;
        if (hotel.getAddress() != null) {
            Address address = modelMapper.map(hotelDto.getAddress(), Address.class);
            Address saveAddress = addressService.saveAddress(address);
            hotel.setAddress(address);
            savedHotel= hotelRepository.save(hotel);
            saveAddress.setHotel(savedHotel);
            addressService.saveAddress(saveAddress);

        } else {
            savedHotel = hotelRepository.save(hotel);

        }
        return modelMapper.map(savedHotel, HotelDto.class);
    }


    protected HotelDto saveHotel(Hotel hotel) {
        return modelMapper.map(hotelRepository.save(hotel), HotelDto.class);
    }


    public AddressDto updateHotelAddress(Long hotelId, AddressDto addressDto) {

        Address address = null;
        Hotel hotel = getHotelById(hotelId);

        if (hotel.getAddress() == null) {
            address = modelMapper.map(addressDto,Address.class);
            Address saveAddress = addressService.saveAddress(address);
            hotel.setAddress(address);
            Hotel savedHotel= hotelRepository.save(hotel);
            saveAddress.setHotel(savedHotel);
            address=addressService.saveAddress(saveAddress);
        } else {

            address = addressService.getAddressById(hotel.getAddress().getId());
            address.setCountry(addressDto.getCountry());
            address.setCity(addressDto.getCity());
            address.setStreet(addressDto.getStreet());
            addressService.saveAddress(address);

        }


        return modelMapper.map(address, AddressDto.class);

    }

    public List<HotelDto> getTopHotels(){
        return  hotelRepository.getTopHotels().stream().map(hotel -> modelMapper.map(hotel,HotelDto.class)).collect(Collectors.toList());
    }


    public HotelDto getHotel(Long hotelId) {
        Hotel hotel = getHotelById(hotelId);
        return modelMapper.map(hotel, HotelDto.class);

    }


    protected Hotel getHotelById(Long id) {
        return hotelRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Hotel not found with this id"));
    }


}
