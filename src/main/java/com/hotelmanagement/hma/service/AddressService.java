package com.hotelmanagement.hma.service;

import com.hotelmanagement.hma.entity.Address;
import com.hotelmanagement.hma.repository.AddressRepository;
import org.springframework.stereotype.Service;

@Service
public class AddressService {
    private final AddressRepository addressRepository;

    public AddressService(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    protected Address getAddressById(Long id) {
        return addressRepository.findById(id).orElseThrow();
    }

    protected Address saveAddress(Address address) {
        return addressRepository.save(address);
    }


}
