package com.hotelmanagement.hma.service;


import com.hotelmanagement.hma.dto.BookingDto;
import com.hotelmanagement.hma.dto.CustomerDto;
import com.hotelmanagement.hma.dto.requests.CustomerExitRequest;
import com.hotelmanagement.hma.entity.Booking;
import com.hotelmanagement.hma.entity.Customer;
import com.hotelmanagement.hma.entity.Room;
import com.hotelmanagement.hma.enums.BookingType;
import com.hotelmanagement.hma.enums.RoomStatus;
import com.hotelmanagement.hma.exception.ResourceNotFoundException;
import com.hotelmanagement.hma.repository.CustomerRepository;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final ModelMapper modelMapper;
    private final RoomService roomService;
    private final BookingService bookingService;

    public CustomerService(CustomerRepository customerRepository, ModelMapper modelMapper, RoomService roomService, BookingService bookingService) {
        this.customerRepository = customerRepository;
        this.modelMapper = modelMapper;
        this.roomService = roomService;
        this.bookingService = bookingService;
    }


    public CustomerDto saveCustomer(CustomerDto customerDto) {
        Customer customer = modelMapper.map(customerDto, Customer.class);
        return modelMapper.map(customerRepository.save(customer), CustomerDto.class);

    }

    public CustomerDto getCustomerById(Long id) {
        Customer customer = findCustomerById(id);
        return modelMapper.map(customer, CustomerDto.class);

    }

    public Page<CustomerDto> getCustomerPage(Pageable pageable) {
        Page<Customer> customerPage = customerRepository.findAll(pageable);
        Page<CustomerDto> customerDtoPage = customerPage.map(customer -> modelMapper.map(customer, CustomerDto.class));
        return customerDtoPage;

    }

    public void deleteCustomerById(Long id) {
        Customer customer = findCustomerById(id);
        customerRepository.delete(customer);
    }


    public BookingDto customerExit(CustomerExitRequest customerExitRequest) {
        Customer customer = findCustomerById(customerExitRequest.getCustomerId());
        Booking booking = customer.getBookings().stream()
                .filter(bkng -> bkng.getId() == customerExitRequest.getBookingId())
                .findFirst()
                .orElseThrow(() -> new ResourceNotFoundException("this customer does not have booking with given id"));

        //make room status empty
        Room room = booking.getRoom();
        room.setRoomStatus(RoomStatus.EMPTY);
        roomService.saveRoom(room);

        // inactivate booking
        booking.setExitDate(LocalDate.now());
        booking.setBookingType(BookingType.EXIT);
        return bookingService.updateBooking(booking);
    }


    protected Customer findCustomerById(Long id) {
        Customer customer = customerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Customer is not available with given info"));
        return customer;

    }

    protected Customer saveCustomer(Customer customer) {
        return customerRepository.save(customer);
    }
}
