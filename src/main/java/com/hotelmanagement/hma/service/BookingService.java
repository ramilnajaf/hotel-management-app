package com.hotelmanagement.hma.service;

import com.hotelmanagement.hma.dto.BookingDto;
import com.hotelmanagement.hma.dto.requests.BookingRequest;
import com.hotelmanagement.hma.entity.Booking;
import com.hotelmanagement.hma.entity.Customer;
import com.hotelmanagement.hma.entity.Hotel;
import com.hotelmanagement.hma.entity.Room;
import com.hotelmanagement.hma.enums.BookingType;
import com.hotelmanagement.hma.enums.RoomStatus;
import com.hotelmanagement.hma.exception.ResourceNotFoundException;
import com.hotelmanagement.hma.exception.RoomNotAvailableException;
import com.hotelmanagement.hma.repository.BookingRepository;
import com.hotelmanagement.hma.utils.BookingUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookingService {


    private final BookingRepository bookingRepository;
    private final ModelMapper modelMapper;
    @Autowired
    private CustomerService customerService;
    private final RoomService roomService;
    private final BookingUtils bookingUtils;
    private final HotelService hotelService;

    public BookingService(BookingRepository bookingRepository, ModelMapper modelMapper, RoomService roomService, BookingUtils bookingUtils, HotelService hotelService) {
        this.bookingRepository = bookingRepository;
        this.modelMapper = modelMapper;
        this.roomService = roomService;
        this.bookingUtils = bookingUtils;
        this.hotelService = hotelService;
    }

    public BookingDto saveBooking(BookingRequest bookingRequest) {
        Customer customer = customerService.findCustomerById(bookingRequest.getCustomerId());

        Hotel hotel = hotelService.getHotelById(bookingRequest.getHotelId());

        Room room = hotel.getRooms().stream().filter(rm -> rm.getId() == bookingRequest.getRoomId())
                .findFirst().orElseThrow(() -> new ResourceNotFoundException("this hotel do not has room with this id"));

        if (bookingUtils.checkAvailabilityOfRoom(room, bookingRequest)) {

            Booking booking = Booking.builder()
                    .commonPrice(bookingUtils.calculateRoomPrice(room, bookingRequest))
                    .fromDate(bookingRequest.getFromDate())
                    .toDate(bookingRequest.getToDate())
                    .bookingType(BookingType.ACTIVE)
                    .build();

            customer.getBookings().add(booking);
            customerService.saveCustomer(customer);

            room.getBookings().add(booking);
            roomService.saveRoom(room);

            booking.setRoom(room);
            booking.setCustomer(customer);

            return modelMapper.map(bookingRepository.save(booking), BookingDto.class);
        } else {
            throw new RoomNotAvailableException("Room is not available for this period");
        }
    }

    protected BookingDto updateBooking(Booking booking) {
        return modelMapper.map(bookingRepository.save(booking), BookingDto.class);
    }


    public BookingDto getBookingByCustomerId(Long customerId, Long bookingId) {
        Customer customer = customerService.findCustomerById(customerId);

        Booking booking = customer.getBookings()
                .stream()
                .filter(bkng -> bkng.getId() == bookingId)
                .findFirst().orElseThrow(
                        () -> new ResourceNotFoundException("this customer does not have booking with given id")
                );

        return modelMapper.map(booking, BookingDto.class);

    }

    public BookingDto getBookingById(Long id) {
        Booking booking = bookingRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Booking not found with given id"));
        return modelMapper.map(booking, BookingDto.class);
    }

    public List<BookingDto> getBookingByRoom(Long roomId) {
       List<Booking> bookingList = bookingRepository.findBookingByRoom(roomService.getRoomByRoomId(roomId));
       return  bookingList.stream().map(booking -> modelMapper.map(booking,BookingDto.class)).collect(Collectors.toList());
    }


}