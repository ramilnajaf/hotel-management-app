package com.hotelmanagement.hma.utils;

import com.hotelmanagement.hma.dto.requests.BookingRequest;
import com.hotelmanagement.hma.entity.Booking;
import com.hotelmanagement.hma.entity.Room;
import com.hotelmanagement.hma.enums.BookingType;
import com.hotelmanagement.hma.repository.BookingRepository;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class BookingUtils {

    private final BookingRepository bookingRepository;

    public BookingUtils(BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    public BigDecimal calculateRoomPrice(Room room, BookingRequest bookingRequest) {
        long daysBetween = ChronoUnit.DAYS.between(bookingRequest.getFromDate(), bookingRequest.getToDate());
        return room.getRoomPrice().multiply(BigDecimal.valueOf(daysBetween));

    }

    public Boolean checkAvailabilityOfRoom(Room room, BookingRequest bookingRequest) {

        List<Booking> bookings = bookingRepository.findAllByRoom(room);
        List<Booking> activeBookings = bookings.stream().filter(booking -> {
            if (
                    (booking.getBookingType().equals(BookingType.ACTIVE))
                            && (bookingRequest.getFromDate().isAfter(booking.getFromDate()) && bookingRequest.getFromDate().isBefore(booking.getToDate()))
                            || (bookingRequest.getToDate().isAfter(booking.getFromDate()) && bookingRequest.getToDate().isBefore(booking.getToDate()))
                            || (bookingRequest.getFromDate().isEqual(booking.getFromDate()) || bookingRequest.getFromDate().isEqual(booking.getToDate())
                               || bookingRequest.getToDate().isEqual(booking.getFromDate()) || bookingRequest.getToDate().isEqual(booking.getToDate()))
                            ||(booking.getFromDate().isAfter(bookingRequest.getFromDate()) && booking.getFromDate().isBefore(bookingRequest.getToDate()))
            ) {
                return true;
            }
            return false;
        }).collect(Collectors.toList());

        if (activeBookings.isEmpty()) {
            return true;
        } else {
            return false;

        }
    }

}
